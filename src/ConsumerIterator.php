<?php

declare(strict_types=1);

namespace Coste\Kafka;

use Iterator;
use RdKafka\Conf;
use RdKafka\Exception;
use RdKafka\KafkaConsumer;
use RdKafka\Message;

/**
 * A consumer for Kafka brokers which is also an iterator.
 *
 * This class provides an iterator on one or several Kafka topics in a synchronous and
 * transactional way. This is not a good thing for performance, but it’s far better if
 * you want something simple and if you don’t want to miss any message.
 *
 * The performance loss may disappear if you are using multiple partitions and consumers.
 *
 * Sample of code :
 *
 *     $consumer = new ConsumerIterator(['topic_1', 'topic_2'], [
 *         'group_id' => 'consumer1',
 *         'brokers' => [
 *             '127.0.0.1:9092',
 *             '127.0.0.2:9092',
 *         ],
 *         'ssl' => [
 *             'ca' => ['location' => '/etc/my_ca.pem'],
 *             'certificate' => ['location' => '/etc/my_certificate.pem'],
 *             'key' => [
 *                 'location' => '/etc/my_key.pem'],
 *                 'password' => 'aabbcc123',
 *             ],
 *         ],
 *     ]);
 *
 *     foreach ($consumer as $key => $message) {
 *         try {
 *
 *             // 1. Processing a message
 *             my_complex_process_which_can_fail($message);
 *
 *         } catch (RuntimeError $e) {
 *
 *             // 2. Some exception in my process occured
 *             put_the_message_somewhere($message);
 *
 *         } catch (Exception $e) {
 *
 *             // 3. If I want, I can still break the loop.
 *             //    If I restart the loop, this message will be read again
 *             //    as it was not treated.
 *             break;
 *         }
 *
 *         // 4. Each loop commits the offset
 *     }
 *
 * @see    http://php.net/manual/en/class.iterator.php
 * @see    https://arnaud-lb.github.io/php-rdkafka/phpdoc/index.html
 *
 * @author Charles-Édouard Coste <charles-edouard@coste.dev>
 */
class ConsumerIterator implements Iterator
{
    const TIMEOUT = 1000;
    private $current;
    private $consumer;
    private $events = [];
    private $topics;
    private $reset;

    /**
     * ConsumerIterator constructor.
     *
     * @param $options
     */
    public function __construct($options)
    {
        $conf = new Conf();

        /**
         * @see https://github.com/edenhill/librdkafka/blob/master/CONFIGURATION.md
         */
        $properties = [
            'enable.auto.commit' => 'false', // We don’t want asynchronous commits
            'enable.partition.eof' => 'true',  // true, by default. But we want to be sure.
            'log.connection.close' => 'false', // Disconnections are common. Don’t worry about it.
            'metadata.broker.list' => implode(',', $options['brokers']),
        ];

        if (!empty($options['ssl'])) {
            $properties += [
                'security.protocol' => 'ssl',
                'ssl.ca.location' => $options['ssl']['ca']['location'] ?? null,
                'ssl.certificate.location' => $options['ssl']['certificate']['location'] ?? null,
                'ssl.key.location' => $options['ssl']['key']['location'] ?? null,
                'ssl.key.password' => $options['ssl']['key']['password'] ?? null,
            ];
        }

        foreach ($properties as $key => $val) {
            $conf->set($key, $val);
        }

        $this->consumer = new KafkaConsumer($conf);

        $this->topics = (array) $options['topics'];
    }

    /**
     * Consume a message.
     *
     * Wait for a timeout or a real message (not signals like "end of partition", "disconnected", etc.)
     *
     * @return Message|null
     *
     * @throws Exception
     */
    private function consume(): Message
    {
        do {
            $message = $this->consumer->consume(self::TIMEOUT);

            /*
             * @see Liste des messages possibles envoyés par la librdkafka :
             * https://github.com/edenhill/librdkafka/blob/0.11.x/src/rdkafka.h#L240
             */
            switch ($message->err) {
                case RD_KAFKA_RESP_ERR__PARTITION_EOF: /* Reached the end of the topic+partition queue on the broker. Not really an error. */
                case RD_KAFKA_RESP_ERR_NO_ERROR: /* Success */
                case RD_KAFKA_RESP_ERR__TIMED_OUT: /* Operation timed out */
                    // those "errors" are not errors
                    break;
                case RD_KAFKA_RESP_ERR_REQUEST_TIMED_OUT: /* Request timed out */
                case RD_KAFKA_RESP_ERR_BROKER_NOT_AVAILABLE: /* Broker not available */
                case RD_KAFKA_RESP_ERR__TRANSPORT: /* Broker transport failure */
                case RD_KAFKA_RESP_ERR_NOT_COORDINATOR_FOR_GROUP: /* Not coordinator for group */
                case RD_KAFKA_RESP_ERR_GROUP_LOAD_IN_PROGRESS: /* Group coordinator load in progress */
                    // those "errors" are temporary and can be ignored
                    break;

                default: // cases that are real errors or that we didn’t list above for now
                    throw new \Exception(rd_kafka_err2str($message->err), $message->err);
            }
        } while (RD_KAFKA_RESP_ERR_NO_ERROR !== $message->err);

        return $message;
    }

    /**
     * Move forward to next element.
     *
     * @throws Exception
     */
    public function next()
    {
        // we commit the last consumed offset
        $this->consumer->commit($this->current);

        $this->current = $this->consume();
    }

    /**
     * Rewind the Iterator to the first unread element.
     *
     * @throws Exception
     */
    public function rewind()
    {
        $this->consumer->subscribe($this->topics);

        $this->current = $this->consume();
    }

    /**
     * Return the current element.
     *
     * @return mixed
     */
    public function current()
    {
        return $this->current->payload;
    }

    /**
     * Return the key of the current element.
     *
     * @return mixed
     */
    public function key()
    {
        return $this->current->key;
    }

    /**
     * Checks if current position is valid.
     *
     * @return bool
     */
    public function valid()
    {
        return !empty($this->current);
    }
}
